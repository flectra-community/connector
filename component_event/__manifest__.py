# Copyright 2019 Camptocamp SA
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html)

{
    "name": "Components Events",
    "version": "2.0.1.0.2",
    "author": "Camptocamp," "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/connector",
    "license": "LGPL-3",
    "category": "Generic Modules",
    "depends": ["component"],
    "external_dependencies": {"python": ["cachetools"]},
    "data": [],
    "installable": True,
}
