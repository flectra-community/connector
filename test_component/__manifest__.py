# Copyright 2019 Camptocamp SA
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html)

{
    "name": "Components Tests",
    "summary": "Automated tests for Components, do not install.",
    "version": "2.0.1.0.0",
    "author": "Camptocamp,Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "category": "Hidden",
    "depends": ["component"],
    "website": "https://gitlab.com/flectra-community/connector",
    "data": ["security/ir.model.access.csv"],
    "installable": True,
    "development_status": "Production/Stable",
    "maintainers": ["guewen"],
}
