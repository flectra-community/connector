# © 2014 David BEAL Akretion, Sodexis
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Connector Base Product",
    "version": "2.0.1.0.0",
    "author": "Flectra Connector Core Editors, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/connector",
    "license": "LGPL-3",
    "category": "Connector",
    "depends": [
        "connector",
        "product",
    ],
    "data": ["views/product_view.xml"],
    "installable": True,
}
