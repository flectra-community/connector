# Flectra Community / connector

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[test_component](test_component/) | 2.0.1.0.0| Automated tests for Components, do not install.
[component](component/) | 2.0.1.1.0| Add capabilities to register and use decoupled components, as an alternative to model classes
[connector_base_product](connector_base_product/) | 2.0.1.0.0| Connector Base Product
[component_event](component_event/) | 2.0.1.0.2| Components Events
[connector](connector/) | 2.0.1.0.0| Connector
[test_connector](test_connector/) | 2.0.1.0.0| Automated tests for Connector, do not install.


